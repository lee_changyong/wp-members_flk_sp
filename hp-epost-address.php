<?php
//regkey
//
//

$regkey = "3213fd220dc877fdc1376049653018"; //$_POST['regkey'];
$target = $_POST['target'];
$query  = $_POST['query'];
$now = array();

function fetch_page($url,$param,$cookies=NULL,$referer_url=NULL){
    if(strlen(trim($referer_url)) == 0) $referer_url= $url;
    $curlsession = curl_init ();
    curl_setopt ($curlsession, CURLOPT_URL, $url);
    curl_setopt ($curlsession, CURLOPT_POST, 1);
    curl_setopt ($curlsession, CURLOPT_POSTFIELDS, $param);
    //curl_setopt ($curlsession, CURLOPT_POSTFIELDSIZE, 0);
    curl_setopt ($curlsession, CURLOPT_TIMEOUT, 60);
    if($cookies && $cookies!=""){
        curl_setopt ($curlsession, CURLOPT_COOKIE, $cookies);
    }
    curl_setopt ($curlsession, CURLOPT_HEADER, 1);
    curl_setopt ($curlsession, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.01; Windows NT 6.0)");
    curl_setopt ($curlsession, CURLOPT_REFERER, "$referer_url"); 

    ob_start();
    $res = curl_exec ($curlsession);
    $buffer = ob_get_contents();
    ob_end_clean();
    $returnVal = array();
    if (!$buffer) {
        $returnVal['error'] = true;
        $returnVal['content'] = "Curl Fetch Error : ".curl_error($curlsession);
    }else{
        $returnVal['error'] = false;
        $returnVal['content'] = $buffer;
    }
    curl_close($curlsession);
    return $returnVal;
}

function get_post_code_xml_by_api($query){
    global $regkey, $target;

    $post_data = array( 'target' => $target, 'regkey' => $regkey, 'query' => iconv('utf-8','euc-kr',$query) );
    $param = http_build_query($post_data);
    $result = fetch_page('http://biz.epost.go.kr/KpostPortal/openapi',$param);
    $result['content'] = remove_none_xml_word($result['content']);
	//echo("<script>console.log('PHP: ".$result['content']."');</script>");
    return $result;
}

function remove_none_xml_word($content){
    $content_array = explode("\n", $content);
    foreach ($content_array as $key => $value) {
        if(substr(trim($value),0,1)!='<'){
            $content_array[$key]='';
        }
    }
    unset($content_array[0]);
    $content = implode("\n", $content_array);
    return trim($content);
}

function add_dash_and_tag_to_postcd($postcd){
    $postcd1=substr($postcd,0,3);
    $postcd2=substr($postcd,3,3);
    return $postcd1.'-'.$postcd2;

}

function print_postcode_table($xml){
    global $now,$target;

    foreach ($xml->itemlist->item as $value) {
		if ($target=="post") {
			$postcd = add_dash_and_tag_to_postcd($value->postcd);
			$postcd = $postcd . ":" . (string)$value->address;
			$now["{$postcd}"] = (string)$value->address;
		} else {
			$postcd = add_dash_and_tag_to_postcd($value->postCd);
			$postcd = $postcd . ":" . (string)$value->lnmAddress;
			$now["{$postcd}"] = (string)$value->rnAddress;
		}
    }

    return $now;
}
function print_postcode_select($xml){
	global $now;
	$tmpArray=array();
	foreach ($xml->itemlist->item as $value) {
		$tmpArray[$value->postcd] = (string)$value->address;
	}
	array_push($now,$tmpArray);
	return $now;
}


if(!empty($query)){
    $result = get_post_code_xml_by_api($query); 
	//echo("<script>console.log('PHP: ".$result['content']."');</script>");
    if ($result['error'] == false){
        $xml = new SimpleXMLElement($result['content']);
        if(count($xml->itemlist->item) == 0){
        	echo $query.'에 대한 검색결과가 없습니다.';
        } else {
           $ret = print_postcode_table($xml);
           echo json_encode( $ret );
        }
    }
    else {
         echo $result['content'];
    }
}


